<?php

use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array (
            0 =>
                array (
                    'id' => 1,
                    'isbn' => 7898592131010,
                    'title' => '360° Literatura',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11603117CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Clenir Bellezi de Oliveira',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Literatura',
                            1 => 'Língua Portuguesa',
                        )),
                    'price' => 239.0,
                    'created_at' => '2018-01-15 09:53:06',
                    'updated_at' => '2018-10-12 10:27:11',
                ),
            1 =>
                array (
                    'id' => 2,
                    'isbn' => 7898592131058,
                    'title' => '360° Produção de Texto',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11603118CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Maria Inês Campos',
                            1 => 'Regina Braz Rocha',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Língua Portuguesa',
                        )),
                    'price' => 219.0,
                    'created_at' => '2018-01-06 22:58:47',
                    'updated_at' => '2018-04-12 02:05:55',
                ),
            2 =>
                array (
                    'id' => 3,
                    'isbn' => 7898592130853,
                    'title' => '360° Gramática',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11604000CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Mauro Ferreira',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Gramática',
                            1 => 'Língua Portuguesa',
                        )),
                    'price' => 249.0,
                    'created_at' => '2018-02-13 12:32:29',
                    'updated_at' => '2018-10-12 16:42:49',
                ),
            3 =>
                array (
                    'id' => 4,
                    'isbn' => 7898592131034,
                    'title' => '360° Matemática',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11615647CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'José Ruy Giovanni',
                            1 => 'José Ruy Giovanni Jr.',
                            2 => 'José Roberto Bonjorno',
                            3 => 'Paulo Roberto Câmara de Sousa',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Matemática',
                        )),
                    'price' => 249.0,
                    'created_at' => '2018-01-30 05:36:28',
                    'updated_at' => '2018-02-02 17:16:13',
                ),
            4 =>
                array (
                    'id' => 5,
                    'isbn' => 7898592130990,
                    'title' => '360° Geografia',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11633072CJ_resized_600x794.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Edilson Adão',
                            1 => 'Laercio Furquim Jr.',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Geografia',
                        )),
                    'price' => 249.0,
                    'created_at' => '2018-01-08 22:44:58',
                    'updated_at' => '2018-09-21 08:41:52',
                ),
            5 =>
                array (
                    'id' => 6,
                    'isbn' => 7898592131096,
                    'title' => '360° Química',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11655078CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Dalton Franco',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Química',
                        )),
                    'price' => 249.0,
                    'created_at' => '2018-01-17 02:40:21',
                    'updated_at' => '2018-05-29 02:35:02',
                ),
            6 =>
                array (
                    'id' => 7,
                    'isbn' => 7898592131096,
                    'title' => '360º Espanhol',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11687325CJM_resized_595x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Henrique Romanos',
                            1 => 'Jacira Paes de Carvalho',
                            2 => 'Zaqueu Machado Borges Júnior',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Espanhol',
                        )),
                    'price' => 229.0,
                    'created_at' => '2018-02-04 13:51:47',
                    'updated_at' => '2018-04-05 21:15:18',
                ),
            7 =>
                array (
                    'id' => 8,
                    'isbn' => 7898592130952,
                    'title' => '360º Filosofia',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11699118CJ_resized_597x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Renato dos Santos Belo',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Filosofia',
                        )),
                    'price' => 219.0,
                    'created_at' => '2018-01-18 21:25:02',
                    'updated_at' => '2018-05-06 15:36:35',
                ),
            8 =>
                array (
                    'id' => 9,
                    'isbn' => 7898592131072,
                    'title' => '360º Sociologia',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11699119CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Agnaldo Kupper',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Sociologia',
                        )),
                    'price' => 219.0,
                    'created_at' => '2018-02-01 02:17:34',
                    'updated_at' => '2018-09-23 20:53:05',
                ),
            9 =>
                array (
                    'id' => 10,
                    'isbn' => 7898592131904,
                    'title' => '360º Inglês',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11687310CJM_resized_600x794.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Flávia da Cruz Miguel',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Inglês',
                        )),
                    'price' => 229.0,
                    'created_at' => '2018-01-13 13:05:55',
                    'updated_at' => '2018-10-29 09:09:50',
                ),
            10 =>
                array (
                    'id' => 11,
                    'isbn' => 7898592130976,
                    'title' => '360º Física',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11655077CJ_resized_600x793.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Claudio Xavier da Silva',
                            1 => 'Benigno Barreto Filho',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Física',
                        )),
                    'price' => 249.0,
                    'created_at' => '2018-01-15 02:18:14',
                    'updated_at' => '2018-06-12 21:31:08',
                ),
            11 =>
                array (
                    'id' => 12,
                    'isbn' => 7898592130914,
                    'title' => '360º Arte',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11640020CJ_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Solange Utuari',
                            1 => 'Daniela Libâneo',
                            2 => 'Fábio Sardo',
                            3 => 'Pascoal Ferrari',
                        )),
                    'level' => 'Ensino médio',
                    'discipline' => json_encode(
                        array (
                            0 => 'Artes',
                        )),
                    'price' => 219.0,
                    'created_at' => '2018-01-08 16:32:48',
                    'updated_at' => '2018-11-19 03:51:50',
                ),
            12 =>
                array (
                    'id' => 13,
                    'isbn' => 9788520000427,
                    'title' => 'Projeto Athos - Ciências - Caderno de Biologia',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11527305_resized_596x800.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'José Trivellato',
                            1 => 'Silvia Trivellato',
                            2 => 'Marcelo Motokane',
                            3 => 'Júlio Foschini Lisboa',
                            4 => 'Carlos Kantor',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'Ciências',
                        )),
                    'price' => 85.0,
                    'created_at' => '2018-01-24 03:32:09',
                    'updated_at' => '2018-05-27 21:11:20',
                ),
            13 =>
                array (
                    'id' => 14,
                    'isbn' => 9788532298393,
                    'title' => 'Projeto Athos - Geografia - 9º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11535004KIT_resized_522x700.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Sonia Castellar',
                            1 => 'Ana Paula Gomes Seferian',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'Geografia',
                        )),
                    'price' => 85.0,
                    'created_at' => '2018-03-19 20:04:48',
                    'updated_at' => '2018-05-01 04:11:58',
                ),
            14 =>
                array (
                    'id' => 15,
                    'isbn' => 9788532298393,
                    'title' => 'Projeto Athos - História - 9º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11534994KIT_resized_522x700.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Joelza Esther',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'História',
                        )),
                    'price' => 185.0,
                    'created_at' => '2018-03-19 20:04:48',
                    'updated_at' => '2018-05-01 04:11:58',
                ),
            15 =>
                array (
                    'id' => 16,
                    'isbn' => 9788532298379,
                    'title' => 'Projeto Athos - Ciências - 9º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11628324KIT_resized_522x700.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'José Trivellato',
                            1 => 'Silvia Trivellato',
                            2 => 'Carlos Kantor',
                            3 => 'Júlio Foschini Lisboa',
                            4 => 'Marcelo Motokane',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'Ciências',
                        )),
                    'price' => 185.0,
                    'created_at' => '2018-03-23 02:07:59',
                    'updated_at' => '2018-08-31 01:24:05',
                ),
            16 =>
                array (
                    'id' => 17,
                    'isbn' => 9788520003671,
                    'title' => 'História, Sociedade & Cidadania - 6º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11539283_resized_600x781.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Alfredo Boulos Júnior',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'História',
                        )),
                    'price' => 181.0,
                    'created_at' => '2018-02-05 15:42:56',
                    'updated_at' => '2018-09-09 06:02:28',
                ),
            17 =>
                array (
                    'id' => 18,
                    'isbn' => 9788520003695,
                    'title' => 'História, Sociedade & Cidadania - 7º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11539284_resized_600x785.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Alfredo Boulos Júnior',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'História',
                        )),
                    'price' => 181.0,
                    'created_at' => '2018-02-18 10:40:31',
                    'updated_at' => '2018-09-25 21:24:31',
                ),
            18 =>
                array (
                    'id' => 19,
                    'isbn' => 9788520003718,
                    'title' => 'História, Sociedade & Cidadania - 8º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11539285_resized_600x781.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Alfredo Boulos Júnior',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'História',
                        )),
                    'price' => 181.0,
                    'created_at' => '2018-01-10 21:18:22',
                    'updated_at' => '2018-06-15 11:06:23',
                ),
            19 =>
                array (
                    'id' => 20,
                    'isbn' => 9788520003732,
                    'title' => 'História, Sociedade & Cidadania - 9º ano',
                    'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11539286_resized_600x783.jpg',
                    'author' => json_encode(
                        array (
                            0 => 'Alfredo Boulos Júnior',
                        )),
                    'level' => 'Ensino Fundamental',
                    'discipline' => json_encode(
                        array (
                            0 => 'História',
                        )),
                    'price' => 181.0,
                    'created_at' => '2018-01-23 00:20:40',
                    'updated_at' => '2018-08-07 15:06:07',
                ),
        );

        \App\Book::insert($data);
    }
}
