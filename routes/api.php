<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('book', 'BookController')->except(['create', 'edit']);

Route::get('clear-cache', function(){
   exec('php artisan cache:clear');
    return response()->json([
        'success' => true,
        'message' => 'Cache eliminado!',
    ], 200);
})->name('clear-cache');