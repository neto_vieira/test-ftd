# Teste FTD

Teste destinado para a vaga de Desenvolvedor PHP Sr


## Sobre o projeto

Simples API Restful de livros

- Costruída em **[Laravel](https://laravel.com)**.
- Configurações de cache.
- Testes unitários para todos os métodos.

## Mapeamento de Rotas

|  Método  |       URI       |      Parâmentros      |     Nome     |                    Ação                     | Middleware |                                                                              Descrição                                                                              | 
| -------  | --------------- | --------------------- | ------------ | ------------------------------------------- | ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| GET/HEAD | api/book        |                       | book.index   | App\Http\Controllers\BookController@index   | api        | Retorna em formato JSON todos os livros do serviço.                                                                                                                 |
| POST     | api/book        |                       | book.store   | App\Http\Controllers\BookController@store   | api        | Registra um novo livro através do serviço, retorna em formato JSON o livro que foi cadastrado com sucesso.                                                          |
| GET      | api/book/{book} | book: Código do livro | book.show    | App\Http\Controllers\BookController@show    | api        | Retorna em formato JSON um livro em específico do serviço, neste caso o livro que corresponde ao id informado.                                                      |
| PUT      | api/book/{book} | book: Código do livro | book.update  | App\Http\Controllers\BookController@update  | api        | Altera o registro de um livro através do serviço, retorna em formato JSON o livro que foi alterado com sucesso, neste caso o livro que corresponde ao id informado. |
| DELETE   | api/book/{book} | book: Código do livro | book.destroy | App\Http\Controllers\BookController@destroy | api        | Remove o registro de um livro através do serviço, retorna em formato JSON o livro que foi removido com sucesso, neste caso o livro que corresponde ao id informado. |
| GET/HEAD | api/clear-cache |                       | clear-cache  | Closure                                     | api        | Limpa o cache dos livros exibidos.                                                                                                                                  |




## Documentações
- [Documentação Laravel](https://laravel.com/docs/5.8).


## Licença

O Laravel é uma framework de código livre licenciada sob o [MIT](https://opensource.org/licenses/MIT).
