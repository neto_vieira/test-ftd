<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'isbn',
        'title',
        'cover',
        'level',
        'author',
        'discipline',
        'price',
    ];

    protected $casts = [
        'author' => 'json',
        'discipline' => 'json',
    ];
}
