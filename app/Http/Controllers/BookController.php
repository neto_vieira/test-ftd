<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class BookController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return Response
 */
    public function index()
    {
        $books = Cache::remember('book.index', 1440 /* One day in minutes*/, function (){
            return Book::all();
        });

        return $books;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param BookRequest $request
     * @return Response
     */
    public function store(BookRequest $request)
    {
        $input = $request->all();
        $book = Book::create($input);
        return $book;
    }


    /**
     * Display the specified resource.
     *
     * @param  Book $book
     *
     * @return Response|Book
     */
    public function show(Book $book)
    {
        return $book;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param BookRequest $request
     * @param  Book $book
     *
     * @return Response
     *
     * @throws \Throwable
     */
    public function update(BookRequest $request, Book $book)
    {
        $input = $request->all();
        $book->fill($input);
        $book->saveOrFail();

        return $book;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Book $book
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return response('Livro removido com sucesso!', 204);
    }
}
