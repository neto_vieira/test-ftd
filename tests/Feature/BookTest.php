<?php

namespace Tests\Feature;

use App\Book;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    /**
     * @return void
     */
    public function testIndex()
    {
        $response = $this->json('GET',route('book.index'));
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testShow()
    {
        $response = $this->json('GET',route('book.show', 1));
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testUnknownBookShow()
    {
        $response = $this->json('GET',route('book.show', 109));
        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function testCreate()
    {
        $data =
            array (
                'isbn' => 9788520103733,
                'title' => 'História, Sociedade & Cidadania - 8º ano',
                'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11539285_resized_600x781.jpg',
                'author' => json_encode(
                    array (
                        0 => 'Alfredo Boulos Júnior',
                    )),
                'level' => 'Ensino Fundamental',
                'discipline' => json_encode(
                    array (
                        0 => 'História',
                    )),
                'price' => 181.0,
                'created_at' => '2018-01-10 21:18:22',
                'updated_at' => '2018-06-15 11:06:23',
            );

        $response = $this->json('POST',route('book.store'), $data);
        $response->assertStatus(201);
    }

    /**
     * @return void
     */
    public function testUpdate()
    {
        $data =
            array (
                'isbn' => 9788520103733,
                'title' => 'UPDATED --- História, Sociedade & Cidadania - 8º ano',
                'cover' => 'https://s3-us-west-2.amazonaws.com/catalogo.ftd.com.br/files/uploads/11539285_resized_600x781.jpg',
                'author' => json_encode(
                    array (
                        0 => 'Alfredo Boulos Júnior',
                    )),
                'level' => 'Ensino Fundamental',
                'discipline' => json_encode(
                    array (
                        0 => 'História',
                    )),
                'price' => 181.0,
                'created_at' => '2018-01-10 21:18:22',
                'updated_at' => '2018-06-15 11:06:23',
            );
        $response = $this->json('PUT',route('book.update', Book::where('isbn', 9788520103733)->first()->id), $data);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testDestroy()
    {
        $response = $this->json('GET',route('book.destroy', Book::where('isbn', 9788520103733)->first()->id));
        $response->assertStatus(200);
    }
}
