<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClearCacheTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testClearCache()
    {
        $response = $this->json('GET',route('clear-cache'));
        $response->assertStatus(200);
    }
}
